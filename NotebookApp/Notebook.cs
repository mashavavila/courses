﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    class Notebook
    {
        List<Note> book = new List<Note>();
        public int idCreate = 1;

        static string znach = null;
        static Notebook nb = new Notebook();
        static void Main(string[] args)
        {
            while (znach != "6")
            {
                Console.Clear();
                Console.WriteLine("Введите номер, соответствующий желаемому действию:");
                Console.WriteLine("1-создание новой записи \n2-редактирование ранее созданной записи\n3-удаление ранее созданной записи\n4-просмотр созданной записи\n5-просмотр всех записей\n6-закрыть телефонную книгу");
                znach = Console.ReadLine();

                switch (znach)
                {
                    case "1":
                        Console.Clear();
                        nb.CreateNewNote();
                        break;


                    case "2":
                        int markTrueNumber = 1, markswitch = 1, markbigswitch = 1;
                        string s;
                        int id = 0;
                        string idS;


                        if (nb.book.Count == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("Ни одна запись не создана. Для начала создайте запись.");
                            Console.ReadKey();
                            break;
                        }
                        while (markbigswitch == 1)
                        {
                            Console.Write("Введите идентификационный номер удаляемой записи. Номер может быть от 1 до {0}:", nb.book.Count);
                            markTrueNumber = 1;
                            while (markTrueNumber == 1)
                            {
                                idS = Console.ReadLine();
                                foreach (var item in idS)
                                {
                                    if (Char.IsNumber(item) != true)
                                    {
                                        Console.WriteLine("Идентифакационный номер должен состоять только из цифр. Введите номер еще раз.");
                                        break;
                                    }
                                    markTrueNumber = 0;
                                }
                                if (markTrueNumber == 0)
                                {
                                    id = Convert.ToInt32(idS);
                                    if ((id < 1) || (id > nb.book.Count))
                                    {
                                        Console.WriteLine("Введите корректное число из диапазона!");
                                        markTrueNumber = 1;
                                    }
                                    else markswitch = 1;
                                }
                            }
                            while ( (markswitch == 1))
                            {
                                Console.Clear();
                                Console.WriteLine("Это та запись, которую вы хотите редактировать?\n");
                                nb.ReadNote(id);
                                Console.WriteLine("\nВыберите:\n1-да, та запись\n2-не та, ввести номер заново\n3-я запутался в своих записях, выведите все\n4-передумал редактировать");
                                s = Console.ReadLine();
                                switch (s)
                                {
                                    case "1":
                                        nb.EditNote(id);
                                        markswitch = 10;
                                        markbigswitch = 0;
                                        break;
                                    case "2":
                                        markswitch = 10;
                                        markbigswitch = 1;
                                        Console.Clear();
                                        break;
                                    case "3":
                                        Console.Clear();
                                        nb.ShowAllNotes();
                                        Console.ReadKey();
                                        markswitch = 10;
                                        markbigswitch = 1;
                                        break;
                                    case "4":
                                        Console.Clear();
                                        Console.WriteLine("Ничего не отредактировано.");
                                        Console.ReadKey();
                                        markswitch = 10;
                                        markbigswitch = 0;
                                        break; 
                                        
                                    default:
                                        Console.Clear();
                                        Console.WriteLine("Введите корректную цифру!");
                                        Console.WriteLine("Нажмите любую клавишу, чтобы продолжить.");
                                        Console.ReadKey();
                                        markswitch = 10;
                                        break;
                                }
                            }
                        }
                        break;


                    case "3":
                        markTrueNumber = 1;
                        markswitch = 1;
                        markbigswitch = 1;
                        s = null;
                        id = 0;
                        idS = null;
                        Console.Clear();

                        if (nb.book.Count == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("Ни одна запись не создана. Для начала создайте запись.");
                            Console.ReadKey();
                            break;
                        }

                        while (markbigswitch == 1)
                        {
                            
                            Console.Write("Введите идентификационный номер удаляемой записи. Номер может быть от 1 до {0}:", nb.book.Count);
                            markTrueNumber = 1;
                            while (markTrueNumber == 1)
                            {
                                idS = Console.ReadLine();
                                foreach (var item in idS)
                                {
                                    if (Char.IsNumber(item) != true)
                                    {
                                        Console.WriteLine("Идентифакационный номер должен состоять только из цифр. Введите номер еще раз.");
                                        break;
                                    }
                                    markTrueNumber = 0;
                                }
                                if (markTrueNumber == 0)
                                {
                                    id = Convert.ToInt32(idS);
                                    if ((id < 1) || (id > nb.book.Count))
                                    {
                                        Console.WriteLine("Введите корректное число из диапазона!");
                                        markTrueNumber = 1;
                                    }
                                    else markswitch = 1;
                                }
                            }
                            while ( (markswitch == 1))
                            {
                                Console.Clear();
                                Console.WriteLine("Это та запись, которую вы хотите удалить?\n");
                                nb.ReadNote(id);
                                Console.WriteLine("\nВыберите:\n1-да, та запись\n2-не та, ввести номер заново\n3-я запутался в своих записях, выведите все\n4-передумал удалять");
                                s = Console.ReadLine();
                                switch (s)
                                {
                                    case "1":
                                        nb.DeleteNote(id);
                                        markswitch = 10;
                                        markbigswitch = 0;
                                        break;
                                    case "2":
                                        markswitch = 10;
                                        markbigswitch = 1;
                                        Console.Clear();
                                        break;
                                    case "3":
                                        Console.Clear();
                                        nb.ShowAllNotes();
                                        Console.ReadKey();
                                        markswitch = 10;
                                        markbigswitch = 1;
                                        break;
                                    case "4":
                                        Console.Clear();
                                        Console.WriteLine("Ничего не удалено.");
                                        Console.ReadKey();
                                        markswitch = 10;
                                        markbigswitch = 0;
                                        break;
                                    default:
                                        Console.Clear();
                                        Console.WriteLine("Введите корректную цифру!");
                                        Console.WriteLine("Нажмите любую клавишу, чтобы продолжить.");
                                        Console.ReadKey();
                                        markswitch = 10;
                                        break;
                                }
                            }
                        }
                        break;




                    case "4":
                        markTrueNumber = 1;
                        id = 1;

                        if (nb.book.Count == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("Ни одна запись не создана. Для начала создайте запись.");
                            Console.ReadKey();
                            break;
                        }
                        
                            Console.Clear();
                            Console.Write("Введите идентификационный номер удаляемой записи. Номер может быть от 1 до {0}:", nb.book.Count);
                            while (markTrueNumber == 1)
                            {
                                idS = Console.ReadLine();
                                foreach (var item in idS)
                                {
                                    if (Char.IsNumber(item) != true)
                                    {
                                        Console.WriteLine("Идентифакационный номер должен состоять только из цифр. Введите номер еще раз.");
                                        markTrueNumber = 1;
                                        break;
                                    }
                                    markTrueNumber = 0;
                                }
                                if (markTrueNumber == 0)
                                {
                                    id = Convert.ToInt32(idS);
                                    if ((id < 1) || (id > nb.book.Count))
                                    {
                                        Console.WriteLine("Введите корректное число из диапазона!");
                                        markTrueNumber = 1;
                                    }
                                }
                            
                            }
                        Console.Clear();
                             nb.ReadNote(id);
                        Console.ReadKey();
                            break;
                    case "5":
                        nb.ShowAllNotes();
                        break;
                    case "6":
                        Console.Clear();
                        Console.WriteLine("Книга будет закрыта после нажатия любой клавиши");
                        break;
                    default:
                        Console.WriteLine("Введите корректное число");
                        break;

                }
            }
        }
        public void CreateNewNote()
        {
            string symb = null, numbS=null;
            int numb = 0, markTrueNumber = 1; ;

            Note page = new Note();
            page.id = idCreate;


            while ((symb != "10")&&(symb!="11"))
            {
                Console.WriteLine("Введите номер, соотвествующий полю, которое хотите заполнить");
                Console.WriteLine("1-фамилия (обязательное поле)\n2-имя (обязательное поле)\n3-отчество \n4-номер телефона (обязательное поле)\n5-страна (обязательное поле)\n6-дата рождения \n7-организация \n8-должность \n9-заметки \n10-всё заполнено \n11-передумал");
                symb = Console.ReadLine();

                switch (symb)
                {
                    case "1":
                        Console.Clear();
                        while (String.IsNullOrWhiteSpace(page.Surname))
                        {
                            Console.Clear();
                            Console.Write("Введите, пожалуйста, фамилию: ");

                            page.Surname = Console.ReadLine();
                        }
                        Console.WriteLine("Заполнена фамилия: " + page.Surname);
                        Console.ReadKey();
                        break;

                    case "2":
                        Console.Clear();
                        while (String.IsNullOrWhiteSpace(page.Name))
                        {
                            Console.Clear();
                            Console.Write("Введите, пожалуйста, имя: ");
                            page.Name = Console.ReadLine();

                        }
                        Console.WriteLine("Заполнено имя: " + page.Name);
                        Console.ReadKey();
                        break;

                    case "3":
                        Console.Clear();
                        if (String.IsNullOrWhiteSpace(page.MiddleName) != true)
                        {
                            Console.WriteLine("Заполнено отчество: " + page.MiddleName);
                            Console.ReadKey();
                        }
                        else
                        {
                            Console.Write("Введите, пожалуйста, отчество: ");
                            page.MiddleName = Console.ReadLine();
                            if (String.IsNullOrWhiteSpace(page.MiddleName) != true)
                            {
                                Console.WriteLine("Заполнено отчество: " + page.MiddleName);
                                Console.ReadKey();
                            }
                        }
                        break;

                    case "4":
                        Console.Clear();
                        while (String.IsNullOrWhiteSpace(page.PhoneNumber))
                        {
                            Console.Clear();
                            Console.Write("Введите, пожалуйста, номер телефона: ");
                            while (markTrueNumber == 1)
                            {
                                numbS = Console.ReadLine();
                                foreach (var item in numbS)
                                {
                                    if (Char.IsNumber(item) != true)
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Номер телефона должен состоять только из цифр. Введите номер еще раз.");
                                        markTrueNumber = 1;
                                        break;
                                    }
                                    markTrueNumber = 0;
                                }
                                if (markTrueNumber == 0)
                                {
                                    page.PhoneNumber = numbS;
                                }
                            }
                        }
                        Console.WriteLine("Заполнен телефонный номер: " + page.PhoneNumber);
                        Console.ReadKey();

                        break;


                    case "5":
                        Console.Clear();
                        while (String.IsNullOrWhiteSpace(page.Country))
                        {
                            Console.Clear();
                            Console.Write("Введите, пожалуйста, страну: ");
                            page.Country = Console.ReadLine();
                        }

                        Console.WriteLine("Заполнена страна: " + page.Country);
                        Console.ReadKey();
                        break;

                    case "6":
                        Console.Clear();
                        if (String.IsNullOrEmpty(page.Data) != true)
                        {
                            Console.WriteLine("Заполнена дата рождения: " + page.Data);
                            Console.ReadKey();
                        }
                        else
                        {
                            Console.Write("Введите, пожалуйста, дату рождения: ");
                            page.Data = Console.ReadLine();
                            if (String.IsNullOrWhiteSpace(page.Data) != true)
                            {
                                Console.WriteLine("Заполнена дата рождения: " + page.Data);
                                Console.ReadKey();
                            }
                        }
                        break;

                    case "7":
                        Console.Clear();
                        if (String.IsNullOrWhiteSpace(page.Organization) != true)
                        {
                            Console.WriteLine("Заполнена организация: " + page.Organization);
                            Console.ReadKey();
                        }
                        else
                        {
                            Console.Write("Введите, пожалуйста, организацию: ");
                            page.Organization = Console.ReadLine();
                            if (String.IsNullOrWhiteSpace(page.Organization) != true)
                            {
                                Console.WriteLine("Заполнена организация: " + page.Organization);
                                Console.ReadKey();
                            }
                        }
                        break;

                    case "8":
                        Console.Clear();
                        if (String.IsNullOrWhiteSpace(page.Job) != true)
                        {
                            Console.WriteLine("Заполнена должность: " + page.Job);
                            Console.ReadKey();
                        }
                        else
                        {
                            Console.Write("Введите, пожалуйста, должность: ");
                            page.Job = Console.ReadLine();
                            if (String.IsNullOrWhiteSpace(page.Job) != true)
                            {
                                Console.WriteLine("Заполнена должность: " + page.Job);
                                Console.ReadKey();
                            }
                        }
                        break;

                    case "9":
                        Console.Clear();
                        if (String.IsNullOrWhiteSpace(page.Notes) != true)
                        {
                            Console.WriteLine("Заполнены заметки: " + page.Notes);
                            Console.ReadKey();
                        }
                        else
                        {
                            Console.Write("Введите, пожалуйста, заметки: ");
                            page.Notes = Console.ReadLine();
                            if (String.IsNullOrWhiteSpace(page.Notes) != true)
                            {
                                Console.WriteLine("Заполнены заметки: " + page.Notes);
                                Console.ReadKey();
                            }
                        }
                        break;

                    case "10":
                        Console.Clear();
                        if ((String.IsNullOrEmpty(page.Surname)))
                        {
                            Console.Write("Фамилия не была введена. Введите, пожалуйста, фамилию: ");
                            while (String.IsNullOrWhiteSpace(page.Surname))
                            {
                                page.Surname = Console.ReadLine();
                            }
                        }
                        if ((String.IsNullOrEmpty(page.Name)))
                        {
                            Console.Write("Имя не было введено. Введите, пожалуйста, имя: ");
                            while (String.IsNullOrWhiteSpace(page.Name))
                            {
                                page.Name = Console.ReadLine();
                            }

                        }
                        if ((String.IsNullOrEmpty(page.PhoneNumber)))
                        {
                            Console.Write("Номер телефона не был введен. Введите, пожалуйста, номер телефона: ");
                            while (String.IsNullOrEmpty(page.PhoneNumber))
                            {
                                page.PhoneNumber = Console.ReadLine();
                            }
                        }
                        if ((String.IsNullOrEmpty(page.Country)))
                        {
                            Console.Write("Страна не была введена. Введите, пожалуйста, страну: ");
                            while (String.IsNullOrWhiteSpace(page.Country))
                            {
                                page.Country = Console.ReadLine();
                            }
                        }
                        Console.Clear();
                        nb.book.Add(page);
                        Console.WriteLine("Создана запись:");
                        nb.ReadNote(idCreate);
                        Console.ReadKey();
                        idCreate++;
                        break;

                    case "11":
                        Console.Clear();
                        Console.WriteLine("Хорошо, новая запись не создана!");
                        Console.ReadKey();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Введите корректный номер!");
                        Console.ReadKey();
                        break;
                }
                Console.Clear();
            }
            


        }
        public void EditNote(int id)
        {
            int mark = 1, index = 0 ;
            int num = 0;
            string symb = null, number;

            foreach (var item in nb.book)
            {
                if (item.id == id)
                {
                    index = nb.book.IndexOf(item);
                    break;
                }
            }

            while (symb != "10")
            {
                Console.Clear();
                Console.WriteLine("Какое поле вы ходите изменить?");
                Console.WriteLine("1-фамилия \n2-имя \n3-отчество \n4-номер телефона \n5-страна \n6-дата рождения \n7-организация \n8-должность \n9-заметки \n10-всё сделано\n");
                nb.ReadNote(id);
                mark = 1;

                while (mark == 1)
                {
                    symb = Console.ReadLine();

                    switch (symb)
                    {
                        case "1":
                            Console.Clear();
                            Console.WriteLine("Старая фамилия: " + book[index].Surname);
                            book[index].Surname = null;
                            while (String.IsNullOrWhiteSpace(book[index].Surname))
                            {
                                Console.Write("Введите, пожалуйста, новую фамилию: ");
                                book[index].Surname = Console.ReadLine();
                            }
                            Console.WriteLine("Заполнена фамилия: " + book[index].Surname);
                            Console.ReadKey();
                            mark = 10;
                            break;

                        case "2":
                            Console.Clear();
                            Console.WriteLine("Старое имя: " + book[index].Name);
                            book[index].Name = null;
                            while (String.IsNullOrWhiteSpace(book[index].Name))
                            {
                                Console.Write("Введите, пожалуйста, имя:");
                                book[index].Name = Console.ReadLine();

                            }
                            Console.WriteLine("Заполнено имя: " + book[index].Name);
                            Console.ReadKey();
                            mark = 10;
                            break;

                        case "3":
                            Console.Clear();
                            Console.WriteLine("Старое отчество: " + book[index].MiddleName);
                            book[index].MiddleName = null;
                            Console.Write("Введите, пожалуйста, отчество:");
                            book[index].MiddleName = Console.ReadLine();
                            if (String.IsNullOrWhiteSpace(book[index].MiddleName) != true)
                            {
                                Console.WriteLine("Заполнено отчество: " + book[index].MiddleName);
                                Console.ReadKey();
                            }
                            mark = 10;

                            break;

                        case "4":
                            Console.Clear();
                            Console.WriteLine("1");
                            Console.WriteLine("Старый номер телефона: " + book[index].PhoneNumber);
                            book[index].PhoneNumber = null;
                            while (String.IsNullOrEmpty(book[index].PhoneNumber))
                            {
                                Console.Write("Введите, пожалуйста, номер телефона:");
                                book[index].PhoneNumber = Console.ReadLine();
                            }
                            Console.WriteLine("Заполнен номер телефона: " + book[index].PhoneNumber);
                            Console.ReadKey();
                            mark = 10;

                            break;

                        case "5":
                            Console.Clear();
                            Console.WriteLine("Старая страна: " + book[index].Country);
                            book[index].Country = null;
                            Console.Write("Введите, пожалуйста, страну:");
                            while (String.IsNullOrWhiteSpace(book[index].Country))
                            {
                                book[index].Country = Console.ReadLine();
                            }
                            Console.WriteLine("Заполнена страна: " + book[index].Country);
                            Console.ReadKey();
                            mark = 10;

                            break;

                        case "6":
                            Console.Clear();
                            Console.WriteLine("Старая дата рождения " + book[index].Data);
                            Console.Write("Введите, пожалуйста, дату рождения:");
                            book[index].Data = (Console.ReadLine());
                            Console.WriteLine("Заполнена дата рождения: " + book[index].Data);
                            Console.ReadKey();
                            mark = 10;

                            break;

                        case "7":
                            Console.Clear();
                            Console.WriteLine("Старая организация: " + book[index].Organization);
                            book[index].Organization = null;
                            Console.Write("Введите, пожалуйста, организацию:");
                            book[index].Organization = Console.ReadLine();
                            Console.WriteLine("Заполнена организация: " + book[index].Organization);
                            Console.ReadKey();
                            mark = 10;

                            break;

                        case "8":
                            Console.Clear();
                            Console.WriteLine("Старая должность: " + book[index].Job);
                            book[index].Job = null;
                            Console.Write("Введите, пожалуйста, должность:");
                            book[index].Job = Console.ReadLine();
                            Console.WriteLine("Заполнена должность: " + book[index].Job);
                            Console.ReadKey();
                            mark = 10;

                            break;

                        case "9":
                            Console.Clear();
                            Console.WriteLine("Старая заметка: " + book[index].Notes);
                            book[index].Notes = null;
                            Console.Write("Введите, пожалуйста, заметки:");
                            book[index].Notes = Console.ReadLine();
                            Console.WriteLine("Заполнена заметка: " + book[index].Notes);
                            Console.ReadKey();
                            mark = 10;

                            break;

                        case "10":
                            mark = 10;
                            Console.Clear();
                            Console.WriteLine("Теперь запись выглядит так:");
                            nb.ReadNote(id);
                            Console.ReadKey();

                            break;
                        default:
                            Console.WriteLine();
                            Console.WriteLine("Введите корректный номер");
                            mark = 10;
                            break;
                    }
                }
            }
        }
        public void DeleteNote(int id)
        {
            foreach (var item in nb.book)
            {
                if (item.id == id)
                {
                    nb.book.RemoveAt(nb.book.IndexOf(item));
                    Console.Clear();
                    Console.WriteLine("Запись была удалена");
                    Console.ReadKey();
                    break;
                }
            }
        }
        public void ReadNote(int id)
        {
            foreach (var item in nb.book)
            {
                if (item.id == id)
                {
                    Console.WriteLine("Идентификационный номер: " + item.id);
                    Console.WriteLine("Фамилия: " + item.Surname);
                    Console.WriteLine("Имя: " + item.Name);
                    Console.WriteLine("Отчество: " + item.MiddleName);
                    Console.WriteLine("Номер телефона: " + item.PhoneNumber);
                    Console.WriteLine("Страна: " + item.Country);
                    Console.WriteLine("Дата рождения: " + item.Data);
                    Console.WriteLine("Организация: " + item.Organization);
                    Console.WriteLine("Должность: " + item.Job);
                    Console.WriteLine("Заметки: " + item.Notes);
                break;
                }
            }
        }
        public void ShowAllNotes()
        {
            Console.Clear();
            if (nb.book.Count == 0)
            {
                Console.WriteLine("Книга пуста. Создайте записи");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Ваша записная книжка:");
                foreach (var item in nb.book)
                {
                    Console.WriteLine("Запись с идентификационным номером " + item.id);
                    Console.WriteLine("Фамилия: " + item.Surname);
                    Console.WriteLine("Имя: " + item.Name);
                    Console.WriteLine("Номер телефона: " + item.PhoneNumber);
                    Console.WriteLine();
                    Console.WriteLine("Нажмите любую клавишу, чтобы продолжить") ;
                }
                
            }
        }
    }
}
