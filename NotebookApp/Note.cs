﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    class Note
    {
        public string surname, name, middleName, phoneNumber, country, organization, job, notes;
        public string data;
        public int id, mark = 0;
        public string Surname { get; set; }
        
        public string Name { get; set; }
        
        public string MiddleName { get; set; }
        
        public string PhoneNumber { get; set; }
       
        public string Country { get; set; }
        
        public string Data { get; set; }

        public string Organization { get; set; }
       
        public string Job { get; set; }
        
        public string Notes { get; set; }
        
    }
}
